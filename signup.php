<!DOCTYPE html>
<html>

<head>
<title>Day04</title>

<style>
input {
  padding: 15px 15px;
  margin: 8px 0;
  box-sizing: border-box;
}
div.b {
  background-color: white;
  padding: 0px;
  border: 2px solid indigo;
  margin: 15px;
}
div.c{
	text-align:center;
}
span.d {
  display:inline-block;
  background-color:limegreen;
  color:white;
  height: 20px;
  width:100px;
  padding: 15px;
  border: 1px solid indigo;
  margin: 15px;
}
.required:after {
	content:" *";
    color: red;
}
span.error{
	color:red;
	padding:20px;
	margin:20px;
}

.button {
  background-color: limegreen;
  border: 1px solid indigo;
  color: white;
  margin:15px;
  width:100px;
  padding: 15px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
}
</style>
</head>

<body>
<?php
$nameErr = $genderErr = $facultyErr = $birthdayErr = "";
  
if ($_SERVER["REQUEST_METHOD"] == "POST") {  
      
    if (empty($_POST["name"])) {  
		$nameErr = "Hãy nhập tên";  
    }
	if (empty ($_POST["gender"])) {  
		$genderErr = "Hãy chọn giới tính";  
    }
	if (empty ($_POST["khoa"])) {  
		$facultyErr = "Hãy chọn phân khoa";  
    }
	if (empty ($_POST["birthday"])) {  
		$birthdayErr = "Hãy nhập ngày sinh";   
    }
}	
?>
	<div class = "b">
		<span class="error">
			<?php
				echo $nameErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $genderErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $facultyErr;
			?>
		</span></br>
		<span class="error">
			<?php
				echo $birthdayErr;
			?>
		</span></br>
		<form method = "post">
			<div class = "container">
				<span class = "d"> <div class = "required"> Họ và tên </div></span>
				<input type="text" id="name" name="name">
			</div>
			<div class = "container">
				<span class = "d"> <div class = "required"> Giới tính </div></span>
				<?php
				$arr1 = array("Nam", "Nữ");
				for ($x = 0; $x < 2; $x++){ ?>
					<input type="radio" id=" <?php echo $x; ?>" name="gender" value="<?php echo $arr1[$x]; ?>">
					<label for="<?php echo $x; ?>"> <?php echo $arr1[$x]; ?> </label>
				<?php } ?>
			</div>
			
			<div class = "container">
				<span class = "d"> <div class = "required"> Phân khoa </div></span>
				<select name="khoa" id="khoa">
				<?php
					$arr = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
					foreach($arr as $key=>$value){ ?>
					<option value = "<?php echo $key; ?>" > <?php echo $value; ?> </option>
				<?php } ?>
				</select>
			</div>
			<div class = "container">
					<span class = "d"> <div class = "required"> Ngày sinh </div></span>
					<input type="date" id="birthday" name="birthday"/>
			</div>
			
			<div class = "container">
					<span class = "d"> Địa chỉ </span>
					<input type="text" id="address" name="address">
			</div>
			<div class = "c">
				<button class = "button">Đăng ký</button>
			</div>
			</form>
		</div>
	</body>
</html>